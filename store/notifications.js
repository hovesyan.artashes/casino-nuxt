export const state = () => {
  return {
    items: [
      {
        description: "We are celebrating over 200 BSV on the jackpot and better payouts for the 3rd, 4th places by giving 500 free Bitto tickets."
      },
      {
        description: "We are celebrating over 200 BSV on the jackpot and better payouts for the 3rd, 4th places by giving 500 free Bitto tickets."
      },
      {
        description: "We are celebrating over 200 BSV on the jackpot and better payouts for the 3rd, 4th places by giving 500 free Bitto tickets."
      },
      {
        description: "We are celebrating over 200 BSV on the jackpot and better payouts for the 3rd, 4th places by giving 500 free Bitto tickets."
      },
      {
        description: "We are celebrating over 200 BSV on the jackpot and better payouts for the 3rd, 4th places by giving 500 free Bitto tickets."
      },
      {
        description: "We are celebrating over 200 BSV on the jackpot and better payouts for the 3rd, 4th places by giving 500 free Bitto tickets."
      },
      {
        description: "We are celebrating over 200 BSV on the jackpot and better payouts for the 3rd, 4th places by giving 500 free Bitto tickets."
      }
    ],
  };
};

export const actions = {
  async getItems({commit, state}) {
    const data = state.items
    commit('SET_ITEMS', data);
  },
};

export const mutations = {
  SET_ITEMS(state, items) {
    state.items = items;
  },
};

export const getters = {
  items: (state) => state.items,
};
