export const state = () => {
  return {
    items: [
      {
        title: 'Bitto',
        img: 'games/bitto.jpg'
      },
      {
        title: 'Baccarat',
        img: 'games/baccarat.jpg'
      },
      {
        title: 'Ladder Game',
        img: 'games/ladder.jpg'
      },
      {
        title: 'Turtle Race',
        img: 'games/turtle.jpg'
      },
      {
        title: 'Bitto',
        img: 'games/bitto.jpg'
      }
    ],
  };
};

export const actions = {
  async getItems({commit, state}) {
    const data = state.items
    commit('SET_ITEMS', data);
  },
};

export const mutations = {
  SET_ITEMS(state, items) {
    state.items = items;
  },
};

export const getters = {
  items: (state) => state.items,
};
